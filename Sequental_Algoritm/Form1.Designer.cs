﻿namespace Sequental_Algoritm
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.CountItems = new System.Windows.Forms.TextBox();
            this.CountEdit = new System.Windows.Forms.Button();
            this.MTLoad = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SvLimiter = new System.Windows.Forms.TextBox();
            this.MakeIt = new System.Windows.Forms.Button();
            this.max = new System.Windows.Forms.TextBox();
            this.min = new System.Windows.Forms.TextBox();
            this.kxc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Количество размещаемых элементов";
            // 
            // CountItems
            // 
            this.CountItems.Location = new System.Drawing.Point(234, 28);
            this.CountItems.Name = "CountItems";
            this.CountItems.Size = new System.Drawing.Size(41, 20);
            this.CountItems.TabIndex = 1;
            // 
            // CountEdit
            // 
            this.CountEdit.Location = new System.Drawing.Point(295, 26);
            this.CountEdit.Name = "CountEdit";
            this.CountEdit.Size = new System.Drawing.Size(84, 23);
            this.CountEdit.TabIndex = 2;
            this.CountEdit.Text = "Подтвердить";
            this.CountEdit.UseVisualStyleBackColor = true;
            this.CountEdit.Click += new System.EventHandler(this.CountEdit_Click);
            // 
            // MTLoad
            // 
            this.MTLoad.Location = new System.Drawing.Point(118, 75);
            this.MTLoad.Name = "MTLoad";
            this.MTLoad.Size = new System.Drawing.Size(95, 64);
            this.MTLoad.TabIndex = 3;
            this.MTLoad.Text = "Загрузить матрицу";
            this.MTLoad.UseVisualStyleBackColor = true;
            this.MTLoad.Click += new System.EventHandler(this.MTLoad_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // SvLimiter
            // 
            this.SvLimiter.Location = new System.Drawing.Point(15, 234);
            this.SvLimiter.Name = "SvLimiter";
            this.SvLimiter.Size = new System.Drawing.Size(52, 20);
            this.SvLimiter.TabIndex = 7;
            // 
            // MakeIt
            // 
            this.MakeIt.Location = new System.Drawing.Point(138, 338);
            this.MakeIt.Name = "MakeIt";
            this.MakeIt.Size = new System.Drawing.Size(137, 23);
            this.MakeIt.TabIndex = 9;
            this.MakeIt.Text = "Произвести расчёт";
            this.MakeIt.UseVisualStyleBackColor = true;
            this.MakeIt.Click += new System.EventHandler(this.MakeIt_Click);
            // 
            // max
            // 
            this.max.Location = new System.Drawing.Point(115, 296);
            this.max.Name = "max";
            this.max.Size = new System.Drawing.Size(52, 20);
            this.max.TabIndex = 10;
            // 
            // min
            // 
            this.min.Location = new System.Drawing.Point(15, 296);
            this.min.Name = "min";
            this.min.Size = new System.Drawing.Size(52, 20);
            this.min.TabIndex = 11;
            this.min.Text = "1";
            // 
            // kxc
            // 
            this.kxc.Location = new System.Drawing.Point(223, 296);
            this.kxc.Name = "kxc";
            this.kxc.Size = new System.Drawing.Size(52, 20);
            this.kxc.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 218);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Максимальное число связей";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 280);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Число вершин в куске";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(73, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "<=X<=";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(220, 266);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Максимальное число кусков";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 407);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.kxc);
            this.Controls.Add(this.min);
            this.Controls.Add(this.max);
            this.Controls.Add(this.MakeIt);
            this.Controls.Add(this.SvLimiter);
            this.Controls.Add(this.MTLoad);
            this.Controls.Add(this.CountEdit);
            this.Controls.Add(this.CountItems);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Компоновка";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CountItems;
        private System.Windows.Forms.Button CountEdit;
        private System.Windows.Forms.Button MTLoad;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox SvLimiter;
        private System.Windows.Forms.Button MakeIt;
        private System.Windows.Forms.TextBox max;
        private System.Windows.Forms.TextBox min;
        private System.Windows.Forms.TextBox kxc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

