﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using WMPLib;

namespace Sequental_Algoritm
{
    public partial class Form1 : Form
    {
        int[,] Matr;
        int[] Pl;
        int[,] step;
        int[] nerasp; //нераспределенные
        int CountIt = -1;
        bool played = false;
        int ColSV = Int32.MaxValue;
        List<Spisok> kusok = new List<Spisok>();
        List<int> kusokP = new List<int>();
         WindowsMediaPlayer WMPs = new WMPLib.WindowsMediaPlayer();
           
        public Form1()
        {
            InitializeComponent();
            WMPs.settings.volume = 100;
            WMPs.URL = "http://chanson.hostingradio.ru:8041/chanson256.mp3?md5=aRrMsR0EP8Cv1q9gr9Kzkg&e=1492863534";
            WMPs.controls.stop();
        }

        private void CountEdit_Click(object sender, EventArgs e)
        {

            try
            {
                CountIt = Convert.ToInt32(CountItems.Text);
                if (CountIt < 1)
                {
                    MessageBox.Show("Недопустимое число элементов");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("Некорретные данные");
                return;
            }
            if (Matr != null)
            {
                Matr = null;
                Pl = null;
                step = null;
                nerasp = null;
            }
            Matr = new int[CountIt, CountIt];
            Pl = new int[CountIt];
            step = new int[CountIt, 2];//0-лок. вес, 1-кол-во кратных вершин
            nerasp = new int[CountIt];
            if (label1.Text != "Количество размещаемых элементов\nГотово")
            {
                label1.Text = label1.Text + "\nГотово";
            }
        }
        private int Minmax()
        {
            int kol = 0, min, addr = 0;
            min = Int32.MaxValue;
            for (int i = 0; i < CountIt; i++)
            {
                if (nerasp[i] == 1)
                {
                    if (step[i, 0] < min)
                    {
                        min = step[i, 0];
                    }
                }
            }
            for (int i = 0; i < CountIt; i++)
            {
                if ((step[i, 0] == min) && (nerasp[i] == 1))
                {
                    kol++;
                }
            }
            if (kol == 0)
            {
                return -1;
            }
            if (kol == 1)
            {
                for (int i = 0; i < CountIt; i++)
                {
                    if ((step[i, 0] == min) && (nerasp[i] == 1))
                    {
                        return i;
                    }
                }
            }
            if (kol > 1)
            {
                int minkr = -1;
                for (int i = 0; i < CountIt; i++)
                {
                    if ((step[i, 0] == min) && (nerasp[i] == 1))
                    {
                        if (step[i, 1] > minkr)
                        {
                            minkr = step[i, 1];
                            addr = i;
                        }
                    }
                }
                return addr;
            }
            return -2;
        }

        private int Svz(int t)
        {
            int sum = 0;
            for (int i = 0; i < kusok[t].Count(); i++)
            {
                sum += step[kusok[t].Ret(i), 0];
            }
            for (int i = 0; i < kusok[t].Count(); i++)
            {
                for (int j = 0; j < CountIt; j++)
                {
                    if ((kusok[t].Contains(j)) && (j != kusok[t].Ret(i)))
                        sum -= Matr[kusok[t].Ret(i), j];
                }
            }
            return sum;
        }

        private int Svz1()
        {
            int sum = 0;
            for (int i = 0; i < CountIt; i++)
            {
                if (nerasp[i] == 1)
                    sum += step[i, 0];
            }
            for (int i = 0; i < CountIt; i++)
            {
                for (int j = 0; j < CountIt; j++)
                {
                    if (nerasp[i] == 1)
                    {
                        if ((nerasp[j] == 1) && (j != i))
                            sum -= Matr[i, j];
                    }
                }
            }
            return sum;
        }
        private int CC()
        {
            int kol = 0;
            for (int i = 0; i < CountIt; i++)
            {
                if (nerasp[i] == 1)
                {
                    kol++;
                }
            }
            return kol;
        }

        private int OchVibor(int t)
        {
            int addr = -1;
            int min = Int32.MaxValue;
            int nowmin, maxv = 0;
            List<int> smez = new List<int>();
            List<int> delt = new List<int>();
            for (int i = 0; i < kusok[t].Count(); i++)
            {
                for (int j = 0; j < CountIt; j++)
                {
                    if ((!kusok[t].Contains(j)) && (Matr[kusok[t].Ret(i), j] != 0) && (!smez.Contains(j)))
                    {
                        smez.Add(j);
                    }
                }
            }
            int sch = 0;
            for (int i = 0; i < smez.Count; i++)
            {
                nowmin = step[smez[i], 0];
                for (int j = 0; j < CountIt; j++)
                {
                    if ((Matr[smez[i], j] != 0) && (kusok[t].Contains(j)))
                    {
                        nowmin -= 2 * Matr[smez[i], j];
                    }
                }
                if (nowmin < min)
                {
                    min = nowmin;
                }
                delt.Add(nowmin);
            }
            for (int i = 0; i < smez.Count; i++)
            {
                if (delt[i] == min)
                {
                    sch++;
                    addr = smez[i];
                }
            }
            if (sch > 1)
            {
                for (int i = 0; i < smez.Count; i++)
                {
                    if ((delt[i] == min) && (step[smez[i], 0] > maxv))
                    {
                        maxv = step[smez[i], 0];
                        addr = smez[i];
                    }
                }
            }
            return addr;
        }
        private void MTLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog {
                Filter = "Текстовые файлы (*.txt)|*.txt|All files (*.*)|*.*",
                FilterIndex = 1
            };
            if (Matr == null)
            {
                MessageBox.Show("Сначала задайте размер!");
                return;
            }
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (StreamReader sr = new StreamReader(openFileDialog1.FileName))
                    {
                        string line;
                        string[] sp;
                        int p = 0;
                        while ((line = sr.ReadLine()) != null)
                        {
                            sp = line.Split(' ');
                            for (int i = 0; i < CountIt; i++)
                            {
                                Matr[p, i] = Convert.ToInt32(sp[i]);
                            }
                            p++;
                        }
                    }
                    for (int i = 0; i < CountIt; i++)
                    {
                        for (int j = 0; j < CountIt; j++)
                        {
                            step[i, 0] += Matr[i, j];
                            if (Matr[i, j] > 1)
                            {
                                step[i, 1]++;
                            }
                        }
                        nerasp[i] = 1;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("В процессе выполнения возникла ошибка. \nOriginal error: " + ex.Message);
                }
            }
        }

        /* private void SpaceLoad_Click(object sender, EventArgs e)
         {
             if (Matr == null)
             {
                 MessageBox.Show("Сначала задайте размер!");
                 return;
             }
             if (openFileDialog1.ShowDialog() == DialogResult.OK)
             {
                 try
                 {
                     using (StreamReader sr = new StreamReader(openFileDialog1.FileName))
                     {
                         string line;
                         string[] sp;
                         line = sr.ReadLine();
                         sp = line.Split(' ');
                         for (int i = 0; i < CountIt; i++)
                             {
                                 Pl[i] = Convert.ToInt32(sp[i]);
                             }  
                     }
                 }
                 catch (Exception ex)
                 {
                     MessageBox.Show("В процессе выполнения возникла ошибка. \nOriginal error: " + ex.Message);
                 }
             }
         }*/

        private void MakeIt_Click(object sender, EventArgs e)
        {
            if (!played)
             {
                 WMPs.controls.play();
                 played = true;
             }
            int nmax, nmin;
            for (int i = 0; i < kusok.Count; i++)
            {
                kusok[i].Clear();
            }
            kusok.Clear();
            for (int i = 0; i < CountIt; i++)
            {
                nerasp[i] = 1;
            }
            int a;
            int t = -1; //1
            int num;
            int kc = Int32.MaxValue;
            List<int> nachalo = new List<int>();
            int teta = 0;
            try
            {
                if (kxc.Text != "")
                {
                    kc = Convert.ToInt32(kxc.Text);
                    if (kc < 1)
                    {
                        MessageBox.Show("Недопустимое значение");
                        return;
                    }
                }
            }
            catch
            {
                kc = Int32.MaxValue;
            }
            try
            {
                ColSV = Convert.ToInt32(SvLimiter.Text);
                if (ColSV < 1)
                {
                    MessageBox.Show("Недопустимое значение");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("Некорректные данные");
                return;
            }
            try
            {
                nmax = Convert.ToInt32(max.Text);
                if (nmax < 1)
                {
                    MessageBox.Show("Недопустимое значение");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("Некорректные данные");
                return;
            }
            try
            {
                nmin = Convert.ToInt32(min.Text);
                if ((nmin < 1) || (nmin > nmax))
                {
                    MessageBox.Show("Недопустимое значение");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("Некорректные данные");
                return;
            }
            bool to2 = true;
            bool to7 = true;
            while (to2)
            {
                to7 = true;
                kusokP.Clear();
                kusok.Add(new Spisok());
                teta = 0;
                t++;
                kusok[t].Clear();
                teta++;
                a = nmax;// 2
                //3 - посчитано при загрузке матрицы
                num = Minmax();//4,5
                if (nachalo.Count - 1 < t)
                    nachalo.Add(num);
                else
                    nachalo[t] = num;//6
                while (to7)
                {
                    if (teta != 1) //7-9
                    {
                        num = OchVibor(t);
                    }
                    if (teta <= a)
                    {
                        kusok[t].Add(num);//10
                        if (Svz(t) <= ColSV)//11
                        {//12
                            kusokP.Clear();
                            for (int i = 0; i < kusok[t].Count(); i++)
                                kusokP.Add(kusok[t].Ret(i));
                        }
                        teta++;//13
                        if (teta <= a)//14
                        {
                            continue; //к 7
                        }
                    }
                    if (kusokP.Count() < nmin)//15 //Важные изменения!
                    {
                        if (t > 0)//21
                        {
                            t = t - 1;//22
                            for (int i = 0; i < kusok[t].Count(); i++)
                            {
                                nerasp[kusok[t].Ret(i)] = 1;
                            }
                            a = kusok[t].Count() - 1;
                            kusok[t].Clear();
                            kusok[t].Add(nachalo[t]);
                            teta = 2;
                            continue;
                        }
                        else
                        {
                            MessageBox.Show("Задача при заданных ограничениях не имеет решения");//23
                            return;
                        }
                    }
                    kusok[t].Clear(); //16
                    for (int i = 0; i < kusokP.Count(); i++)
                        kusok[t].Add(kusokP[i]);
                    for (int i = 0; i < kusok[t].Count(); i++)
                    {
                        nerasp[kusok[t].Ret(i)] = 0;
                    }
                    a = nmax;
                    if (CC() > nmax)//17
                    {
                        if (t + 1 < kc - 1)//20
                        {
                            to7 = false;
                            continue;//ко 2-му
                        }
                        else
                        {
                            MessageBox.Show("Задача при заданных ограничениях не имеет решения");//23
                            return;
                        }
                    }
                    if (CC() < nmin)//18
                    {
                        if (t > 0)//21
                        {
                            t = t - 1;//22
                            for (int i = 0; i < kusok[t].Count(); i++)
                            {
                                nerasp[kusok[t].Ret(i)] = 1;
                            }
                            a = kusok[t].Count() - 1;
                            kusok[t].Clear();
                            kusok[t].Add(nachalo[t]);
                            teta = 2;
                            continue;
                        }
                        else
                        {
                            MessageBox.Show("Задача при заданных ограничениях не имеет решения");//23
                            return;
                        }
                    }
                    if (Svz1() > ColSV)//19
                    {
                        if (t > 0)//21
                        {
                            t = t - 1;//22
                            for (int i = 0; i < kusok[t].Count(); i++)
                            {
                                nerasp[kusok[t].Ret(i)] = 1;
                            }
                            a = kusok[t].Count() - 1;
                            kusok[t].Clear();
                            kusok[t].Add(nachalo[t]);
                            teta = 2;
                            continue;
                        }
                        else
                        {
                            MessageBox.Show("Задача при заданных ограничениях не имеет решения");//23
                            return;
                        }
                    }
                    else
                    {
                        to7 = false;
                        to2 = false;//24
                        kusok.Add(new Spisok());
                        for (int i = 0; i < CountIt; i++)
                        {
                            if (nerasp[i] == 1)
                            {
                                kusok[t + 1].Add(i);
                            }
                        }
                    }
                }
            }
            SaveFileDialog saveFileDialog1 = new SaveFileDialog {
                FileName = "",
                DefaultExt = ".txt",
                Filter = "text file|*.txt"
            };   
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string line;
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(saveFileDialog1.FileName))
                {
                    for (int i = 0; i < kusok.Count; i++)
                    {
                        line = "Кусок " + (i + 1).ToString() + ": ";
                        for (int j = 0; j < kusok[i].Count(); j++)
                        {
                            line += (kusok[i].Ret(j) + 1).ToString() + " ";
                        }
                        line += "(Соединений попало в разрез: " + Svz(i).ToString() + ")";
                        file.WriteLine(line);
                    }

                }
            }
        }

    }
    public class Spisok
    {
        List<int> data;
        public Spisok()
        {
                data= new List<int>();

        }
        public void Add(int v)
        {
            data.Add(v);
        }
        public int Ret(int n)
        {
            return data[n];
        }
        public void Clear()
        {
            data.Clear();
        }
        public bool Contains(int v)
        {
            return data.Contains(v);
        }
        public int Count()
        {
            return data.Count;
        }
    }
}
